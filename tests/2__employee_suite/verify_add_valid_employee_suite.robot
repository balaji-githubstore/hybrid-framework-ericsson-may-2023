*** Comments ***
1. Navigate to the url
 2. Enter Admin username
 3. Enter password
 4. Click Login
 5. Mouse over on PIM
 6. Click on Add Employee
 7. Enter firstname
 8. Enter middlename
 9. Enter lastname
 10. Upload the employee image
 11. Click on save"

 Data:
 1. Username: Admin
 2. Password: admin123
 3. Firstname: John
 4. MiddleName: J
 5. Last Name: Wick
 6.Employee Id: 4545
 7.Upload image: "C:\Users\JiDi\Pictures\Screenshots\Screenshot (1).png"

 1. Add Employee header should be displayed
 2. Employee First Name should be displayed in the text box

*** Settings ***
Documentation   This suite file verifies valid users are able to login to the dashboard
...     and connected to test case TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

*** Test Cases ***
Verify Add Valid Employee Test
    Input Text    name=username    Admin
    Input Text    name=password    admin123
    Click Element    xpath=//button[normalize-space()='Login']
#     5. Mouse over on PIM
# 6. Click on Add Employee
# 7. Enter firstname
# 8. Enter middlename
# 9. Enter lastname
# 10. Upload the employee image
# 11. Click on save"
#1. Added Employee Profile header should be displayed
# 2. Employee First Name should be displayed in the text box